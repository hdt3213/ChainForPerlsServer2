import MySQLdb
from sqlalchemy import create_engine, Table, Column, Integer, String, MetaData, ForeignKey
import config

from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, backref

metadata = MetaData()

Base = declarative_base(metadata=metadata)


class User(Base):
    __tablename__ = 'user'
    user_id = Column('user_id', Integer, autoincrement=True, primary_key=True)
    username = Column('username', String(32))
    password = Column('password', String(32))
    success = Column('success', Integer)
    failure = Column('failure', Integer)
    score = Column('score', Integer)

db = 'mysql://%s:%s@%s:%d/%s?charset=utf8' % (config.DB_USER, config.DB_PWD, config.DB_HOST, config.DB_PORT, config.DB_NAME)


def get_session():
    engine = create_engine(db, encoding="utf-8", echo=False)
    mysql_session_maker = sessionmaker(bind=engine)
    session = mysql_session_maker()
    return session

sql_session = get_session()

if __name__ == '__main__':
    print(sql_session.query(User).count())
