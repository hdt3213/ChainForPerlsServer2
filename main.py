import tornado.escape
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.websocket


from handler import PushHandler, EchoHandler, GameHandler, IndexHandler
from config import STATIC_PATH, TEMPLATE_PATH

from tornado.options import define, options

define("port", default=8080, help="run on the given port", type=int)


class Application(tornado.web.Application):
    def __init__(self):
        handlers = [
            (r"/game", GameHandler),
            (r"/push", PushHandler),
            (r"/echo", EchoHandler),
            (r"/index", IndexHandler),
            (r"/", IndexHandler),
        ]
        tornado.web.Application.__init__(self, handlers, template_path=TEMPLATE_PATH)


def main():
    tornado.options.parse_command_line()
    app = Application()
    app.listen(8080)
    tornado.ioloop.IOLoop.instance().start()


if __name__ == "__main__":
    main()
