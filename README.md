基于html5的在线五子棋游戏, 使用weui作为前端框架, 使用tornado开发后端.

使用http协议进行通信, 使用GET方法获得网页, 主动行为使用POST提交, 服务端通过websocket推送事件.

# main.py

初始化tornado.web.Application, 监听事件循环.

# handler.py

## IndexHandler

将获取游戏界面的请求重定向到静态文件`/static/index.html`.

## GameHandler

处理通过POST提交的主动行为并交由service处理.

## EchoHandler

测试用WebsocketHandler返回用户发送的信息.

## PushHandler

用于推送事件的WebsocketHandler, 用户在收到登录成功的响应后发起socket连接, PushHandler会返回连接建立的结果.

# service.py

提供服务的控制器函数, 调用cache.py操作缓存, 调用query.py操作数据库, 调用push.py推送事件.

# index.html

主要的游戏界面.

# game.js

维护一个通用的游戏模型, 调用judge.js进行胜利判断,不依赖其他js脚本.

# jude.js

胜利判断算法.

# driver.js

进行网络通信的脚本, 当收到服务端推送的消息时调用index.js中的update view更新界面不直接访问DOM元素.

# index.js

前端核心逻辑共由三部分组成:

- 初始化部分

- 更新界面: 其它文件通过这部分函数更新界面不直接访问DOM书

- 事件处理器: 响应用户点击等事件